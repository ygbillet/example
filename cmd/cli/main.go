package main

import (
	"fmt"
	"os"
	"strconv"

	"bitbucket.org/ygbillet/example"
)

var url string

func main() {

	URLs := os.Args[1:]

	padding := strconv.Itoa(getMaxLength(URLs))

	for _, URL := range URLs {
		title, err := example.CollectTitle(URL)
		if err != nil {
			fmt.Printf(err.Error()) // Print error as string
			return
		}
		fmt.Printf("%"+padding+"s | %s\n", URL, title)
	}

}

func getMaxLength(ts []string) int {
	var max int
	for _, s := range ts {
		if len(s) > max {
			max = len(s)
		}
	}

	return max
}
