package example

import "testing"

func TestExtractTitle(t *testing.T) {

	var flagtests = []struct {
		html  string
		title string
	}{
		{"<html><head><title>Hello</title></head><body></body></html>", "Hello"},
		{"<html><head><title></title></head><body></body></html>", ""},
		{"<html><body></body></html>", ""},
		{"<html><body><head><title>Hello</title></head></body></html>", "Hello"},
	}

	for _, tt := range flagtests {
		r := extractTitle([]byte(tt.html))
		if r != tt.title {
			t.Errorf("extractTitle(%s): expected %s, actual %s", tt.html, tt.title, r)
		}
	}
}
