package example

import (
	"bytes"
	"log"

	"github.com/PuerkitoBio/goquery"
)

func extractTitle(html []byte) string {
	reader := bytes.NewReader(html)
	doc, err := goquery.NewDocumentFromReader(reader)
	if err != nil {
		log.Println(err)
	}
	return doc.Find("title").Text()
}
