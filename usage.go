package example

// CollectTitle get content of title attribut from an URL
func CollectTitle(url string) (string, error) {
	h, err := get(url)
	if err != nil {
		return "", err
	}
	return extractTitle(h), nil
}
